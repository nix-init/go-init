{ buildGoModule }:

let
  # replace with name of project
  pname = "<project-name>";
  version = "0.0.1";
in buildGoModule {
  inherit pname version;

  # replace with git repo once created
  src = ./.;

  # replace with hash created in `nix-build` error
  vendorHash = "";
}
